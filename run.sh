#!/usr/bin/env sh

python manage.py migrate --noinnput

DJANGO_SUPERUSER_USERNAME='admin' \
  DJANGO_SUPERUSER_PASSWORD='admin' \
  python manage.py createsuperuser --noinnput

python manage.py collectstatic --noinput

python manage.py runserver --noreload 0.0.0.0:8000